import { createContext } from 'react'
import {
  ADD_JOB,
  REMOVE_JOB,
  SET_ERROR,
  SET_JOB,
  SET_JOB_NAME_EN,
  SET_JOB_NAME_TH,
  SET_ONSEARCH,
  SET_SEARCH_QUERY,
  SET_JOB_DESCRIPTION_EN,
  SET_JOB_DESCRIPTION_TH,
  SET_JOB_CATEGORY,
  SET_JOB_SALARY,
  SET_OPTIONAL,
  SET_JOB_FORM,
  RESET_FORM,
  SET_LANG,
  SET_JOB_TYPES,
} from './types'

const initailState = {
  jobs: [],
  onSearch: false,
  jobTypes: [],
  searchQuery: {
    keyword: '',
    lang: 'th',
    categoryId: null,
    salary: null,
    workFromHome: null,
  },
  error: null,
  job: {
    _id: '',
    name: {
      th: '',
      en: '',
    },
    description: {
      th: '',
      en: '',
    },
    categoryId: null,
    salary: null,
    optional: {
      workFromHome: false,
    },
  },
  lang: 'en',
}

function jobReducer(state, action) {
  switch (action.type) {
    case SET_JOB:
      return {
        ...state,
        jobs: action.payload,
      }
    case ADD_JOB:
      return {
        ...state,
        jobs: [...state.jobs, action.payload],
      }
    case REMOVE_JOB:
      return {
        ...state,
        jobs: state.jobs.filter((job) => job._id !== action.payload),
      }
    case SET_ONSEARCH:
      return {
        ...state,
        onSearch: action.payload,
      }
    case SET_SEARCH_QUERY:
      return {
        ...state,
        searchQuery: action.payload,
      }
    case SET_ERROR:
      return {
        ...state,
        error: action.payload,
      }
    case SET_JOB_NAME_TH:
      return {
        ...state,
        job: {
          ...state.job,
          name: {
            ...state.job.name,
            th: action.payload,
          },
        },
      }
    case SET_JOB_NAME_EN:
      return {
        ...state,
        job: {
          ...state.job,
          name: {
            ...state.job.name,
            en: action.payload,
          },
        },
      }
    case SET_JOB_DESCRIPTION_TH:
      return {
        ...state,
        job: {
          ...state.job,
          description: {
            ...state.job.description,
            th: action.payload,
          },
        },
      }
    case SET_JOB_DESCRIPTION_EN:
      return {
        ...state,
        job: {
          ...state.job,
          description: {
            ...state.job.description,
            en: action.payload,
          },
        },
      }
    case SET_JOB_CATEGORY:
      return {
        ...state,
        job: {
          ...state.job,
          categoryId: action.payload,
        },
      }
    case SET_JOB_SALARY:
      return {
        ...state,
        job: {
          ...state.job,
          salary: action.payload,
        },
      }
    case SET_OPTIONAL:
      return {
        ...state,
        job: {
          ...state.job,
          optional: action.payload,
        },
      }
    case SET_JOB_FORM:
      return {
        ...state,
        job: action.payload,
      }
    case SET_LANG:
      return {
        ...state,
        lang: action.payload,
      }
    case SET_JOB_TYPES:
      return {
        ...state,
        jobTypes: action.payload,
      }
    case RESET_FORM:
      return {
        ...state,
        job: initailState.job,
      }
    default:
      throw new Error()
  }
}

const JobContext = createContext({})

export { initailState, jobReducer, JobContext }
