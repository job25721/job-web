import { useContext } from 'react'
import PropTypes from 'prop-types'
import { Empty, Button, Skeleton } from 'antd'
import { useTranslation } from 'next-i18next'
import JobCard from './JobCard'
import { JobContext } from '../../lib/reducer'
import { Card } from '../style'

const LoadingCard = () => (
  <Card
    bordered={false}
    style={{
      marginTop: 20, borderRadius: 20, backgroundColor: 'whitesmoke',
    }}
  >
    {/* {[80, 60, 75, 50].map((w) => (
      <div
        key={w}
        style={{
          width: `${w}%`, height: 10, backgroundColor: '#cccccc', marginTop: 12,
        }}
      />
    ))} */}
    <Skeleton active />
  </Card>
)

const JobList = ({ loading }) => {
  const { state } = useContext(JobContext)
  const { t } = useTranslation('common')
  if (state.error) {
    return state.error
  }

  return (
    <Card style={{ overflow: 'auto' }} height={800} bordered={false}>

      {loading ? [...Array(Math.floor(Math.random() * 3) + 1)].map((_, i) => (
        <LoadingCard key={i.toString()} />
      )) : state.jobs.map((job, i) => (
        <JobCard job={job} mt={i !== 0 ? 20 : 0} key={job._id.toString()} />
      ))}
      {!loading && state.jobs.length === 0 && (
      <Empty
        description={(
          <span>
            {t('noJob')}
          </span>
    )}
      >
        <Button primary="true" shape="round" type="primary">
          {t('createNew')}
        </Button>
      </Empty>
      )}
    </Card>
  )
}

JobList.propTypes = {
  loading: PropTypes.bool.isRequired,
}

export default JobList
