import { useQuery } from '@apollo/client'
import {
  Button, Col, Row, Select, Switch,
} from 'antd'
import { SearchOutlined } from '@ant-design/icons'
import Text from 'antd/lib/typography/Text'
import { useContext, useEffect } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { useTranslation } from 'next-i18next'

import { useRouter } from 'next/router'
import { JobContext } from '../../lib/reducer'

import { getJobTypes } from '../../service/gateway/query/job'
import { Input, Card } from '../style'
import { SET_SEARCH_QUERY } from '../../lib/types'

const CustomRow = styled(Row)`
  flex-direction : column ;
`

const SearchBox = ({ loadJob }) => {
  const { state, dispatch } = useContext(JobContext)
  const router = useRouter()
  const { searchQuery } = state

  const { data, refetch } = useQuery(getJobTypes, {
    variables: {
      lang: router.locale,
    },
  })

  const { t } = useTranslation('searchBox')

  useEffect(() => {
    refetch()
  }, [router.locale])

  return (
    <Card bordered={false}>
      <Row>
        <Text>{t('searchKeyLabel')}</Text>
        <Input
          value={searchQuery.keyword}
          onChange={({ target }) => dispatch({ type: SET_SEARCH_QUERY, payload: { ...searchQuery, keyword: target.value } })}
          type="text"
          rounded="true"
        />
      </Row>
      <CustomRow>
        <Text>{t('category')}</Text>
        <Col span={24}>
          <Select
            value={searchQuery.categoryId}
            onChange={(categoryId) => dispatch({ type: SET_SEARCH_QUERY, payload: { ...searchQuery, categoryId } })}
            style={{ width: '100%' }}
          >
            {
              data && (
              <>
                <Select.Option value={null}>{t('all')}</Select.Option>
                {data.getJobTypes.data.map((type) => (
                  <Select.Option key={type.id} value={type.id}>{type.name}</Select.Option>
                ))}
              </>
              )
            }
          </Select>
        </Col>
      </CustomRow>
      <Row>
        <Text>
          {t('salary')}
        </Text>
        <Input
          value={searchQuery.salary}
          onChange={({ target }) => dispatch({
            type: SET_SEARCH_QUERY,
            payload: { ...searchQuery, salary: target.value !== '' ? Math.abs(target.value) : null },
          })}
          type="number"
          rounded="true"
        />
      </Row>
      {/* <Row align="middle" style={{ marginTop: 10 }}>
        <Switch onChange={(checked) => dispatch({ type: SET_SEARCH_QUERY, payload: { ...searchQuery, workFromHome: !checked ? null : false } })} />
        <Text style={{ paddingLeft: 10 }}>{t('option')}</Text>
      </Row> */}
      <Row align="middle" style={{ marginTop: 10 }}>
        <Switch
          checked={searchQuery.workFromHome}
          onChange={(checked) => dispatch({ type: SET_SEARCH_QUERY, payload: { ...searchQuery, workFromHome: checked || null } })}
        />
        <Text style={{ paddingLeft: 10 }}>{t('workFromHome')}</Text>
      </Row>
      <Row style={{ marginTop: 10 }} justify="center">
        <Button
          type="primary"
          icon={<SearchOutlined />}
          shape="round"
          onClick={() => loadJob({ variables: { ...searchQuery, lang: router.locale } })}
        >
          {t('searchKeyLabel')}

        </Button>
      </Row>
    </Card>

  )
}

SearchBox.propTypes = {
  loadJob: PropTypes.func.isRequired,
}

export default SearchBox
