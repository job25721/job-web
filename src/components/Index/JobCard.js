import {
  Row, Typography, Col, Tag, Card, Button, Modal,
} from 'antd'
import {
  HomeOutlined, EditOutlined, DeleteOutlined, DollarOutlined, ExclamationCircleOutlined,
} from '@ant-design/icons'
import Text from 'antd/lib/typography/Text'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import Link from 'next/link'
import { useMutation } from '@apollo/client'
import { useContext } from 'react'
import { useRouter } from 'next/router'
import { useTranslation } from 'next-i18next'
import { deleteJobByid } from '../../service/gateway/mutation/job'
import { JobContext } from '../../lib/reducer'
import { REMOVE_JOB } from '../../lib/types'

const { confirm } = Modal

const { Title } = Typography

const OptionCol = styled(Col)`
  display : flex;
  flex-direction : column;
  align-items : flex-end;
  justify-content : space-between;
`

const JobCard = ({ mt = 0, job }) => {
  const router = useRouter()
  const [DeleteJobById] = useMutation(deleteJobByid)
  const { dispatch } = useContext(JobContext)
  const { t } = useTranslation('jobCard')
  const success = (msg) => {
    Modal.success({
      content: msg,
    })
  }
  const error = (msg) => {
    Modal.error({
      title: msg,
    })
  }
  const deleteJob = async () => {
    try {
      await DeleteJobById({ variables: { id: job._id } })
      dispatch({ type: REMOVE_JOB, payload: job._id })
      success(t('deleteComplete'))
    } catch (err) {
      error(err.message)
    }
  }
  const showConfirm = () => {
    confirm({
      title: t('confirmDelete'),
      icon: <ExclamationCircleOutlined />,
      onOk: () => deleteJob(),
    })
  }

  return (
    <Card style={{ marginTop: mt, borderRadius: 20 }}>
      <Row>
        <Col flex={1}>
          <Row style={{ flexDirection: 'column' }}>
            <Title level={3}>{job.name[`${router.locale}`]}</Title>
            <Text>
              {t('category')}
              {' '}
              :
              {' '}
              {job.category.name}
            </Text>
            <Text>
              {t('description')}
              {' '}
              :
              {' '}
              {job.description[`${router.locale}`]}
            </Text>
          </Row>

          {
         job.optional.workFromHome && (
         <Row>
           <Tag style={{ borderRadius: 20 }} color="volcano">
             <HomeOutlined />
             <Text style={{ paddingLeft: 5 }}>{t('workFromHome')}</Text>
           </Tag>
         </Row>
         )
       }
        </Col>
        <OptionCol>
          <Row>
            <Link href={`/manage/${job._id}`}>
              <Button icon={<EditOutlined />} shape="round" style={{ marginRight: 10 }}>{t('edit')}</Button>
            </Link>
            <Button onClick={showConfirm} icon={<DeleteOutlined />} shape="round" danger>{t('delete')}</Button>
          </Row>
          <Row style={{ alignItems: 'center' }}>
            <DollarOutlined />
            <Text style={{ paddingLeft: 5 }}>
              {job.salary ? `${job.salary} ${t('THB')}` : t('salaryNone')}
            </Text>
          </Row>
        </OptionCol>
      </Row>
    </Card>
  )
}

JobCard.propTypes = {
  mt: PropTypes.number.isRequired,
  job: PropTypes.shape({
    _id: PropTypes.string,
    name: PropTypes.shape({
      th: PropTypes.string,
      en: PropTypes.string,
    }),
    description: PropTypes.shape({
      th: PropTypes.string,
      en: PropTypes.string,
    }),
    salary: PropTypes.number,
    categoryId: PropTypes.number,
    category: PropTypes.shape({
      id: PropTypes.number,
      name: PropTypes.string,
      code: PropTypes.string,
    }).isRequired,
    optional: PropTypes.shape({
      workFromHome: PropTypes.bool,
    }),

  }).isRequired,
}

JobCard.defaultProps = {

}

export default JobCard
