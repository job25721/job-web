import { useMutation, useQuery } from '@apollo/client'
import {
  Button, Row, Switch, Modal, Form, Select,
} from 'antd'
import { useContext } from 'react'
import { useRouter } from 'next/router'
import PropTypes from 'prop-types'
import { ExclamationCircleOutlined } from '@ant-design/icons'
import { useTranslation } from 'next-i18next'
import { useForm } from 'antd/lib/form/Form'

import { JobContext } from '../../lib/reducer'
import {
  ADD_JOB,
} from '../../lib/types'
import { createJob, updateJobById } from '../../service/gateway/mutation/job'
import { getJobTypes } from '../../service/gateway/query/job'
import { Card, Input } from '../style'

const { confirm } = Modal
const JobForm = ({ jobData }) => {
  const [form] = useForm()
  const router = useRouter()
  const { dispatch } = useContext(JobContext)

  const [updateJob] = useMutation(updateJobById)
  const [createNewJob] = useMutation(createJob)
  const { t } = useTranslation('jobForm')

  const { data } = useQuery(getJobTypes, {
    variables: {
      lang: router.locale,
    },
  })
  const success = (msg) => {
    Modal.success({
      content: msg,
      onOk: () => router.push(`/${router.locale}`),
    })
  }
  const error = (msg) => {
    Modal.error({
      title: msg,
    })
  }

  const discardForm = () => {
    if (form.isFieldsTouched()) {
      Modal.confirm({
        content: t('confrimDiscard'),
        onOk: () => router.push(`/${router.locale}`),
      })
    } else {
      router.push(`/${router.locale}`)
    }
  }
  const formSubmit = async (values) => {
    const {
      name, description, categoryId, salary, optional,
    } = values
    try {
      const updateField = {
        nameTh: name.th,
        nameEn: name.en,
        desTh: description.th,
        desEn: description.en,
        categoryId,
        salary,
        workFromHome: optional.workFromHome,
      }

      if (jobData) {
        await updateJob({ variables: { id: jobData._id, ...updateField } })
        success(t('updateSuccess'))
      } else {
        const createdJob = await createNewJob({ variables: updateField })
        if (createdJob.errors) {
          throw createdJob.errors
        }
        if (createdJob) {
          dispatch({ type: ADD_JOB, payload: createdJob.data.createJob })
          success(t('successAlert'))
        }
      }
    } catch (err) {
      error(err.message)
    }
  }

  const onPromiseConfirm = async () => {
    try {
      const values = await form.validateFields()
      confirm({
        title: `${t('confirm')} ?`,
        icon: <ExclamationCircleOutlined />,
        onOk: () => formSubmit({ ...values, salary: +values.salary }),
      })
    } catch (err) {
      error(err.msg)
    }
  }

  const { Option } = Select

  return (
    <Card bordered={false}>
      <Form
        initialValues={jobData}
        form={form}
      >
        <Form.Item
          label={t('nameTh')}
          name={['name', 'th']}
          rules={[{ required: true, message: 'Please input name th' }]}
        >
          <Input rounded="true" />
        </Form.Item>
        <Form.Item
          label={t('nameEn')}
          name={['name', 'en']}
          rules={[{ required: true, message: 'Please input name en' }]}
        >
          <Input rounded="true" />
        </Form.Item>
        <Form.Item
          label={t('descriptionTh')}
          name={['description', 'th']}
          rules={[{ required: true, message: 'Please input description th' }]}
        >
          <Input rounded="true" />
        </Form.Item>
        <Form.Item
          label={t('descriptionEn')}
          name={['description', 'en']}
          rules={[{ required: true, message: 'Please input description en' }]}
        >
          <Input rounded="true" />
        </Form.Item>
        <Form.Item name="categoryId" label={t('category')} rules={[{ required: false }]}>
          <Select
            placeholder="Select a option and change input text above"
            onChange={(value) => form.setFieldsValue({ category: value })}

          >
            {data && data.getJobTypes.data.map((item) => (
              <Option key={item.code} value={item.id}>{item.name}</Option>
            ))}
          </Select>
        </Form.Item>
        <Form.Item
          label={t('salary')}
          name={['salary']}
          rules={[{
            validator: async (rule, value) => {
              if (value < 0) {
                throw new Error('salary must be above or equal zero')
              }
            },
          }]}
        >
          <Input
            onChange={(e) => {
              if (e.target.value === '') {
                form.setFieldsValue({ salary: null })
              }
            }}
            type="number"
            rounded="true"
          />
        </Form.Item>

        <Form.Item name={['optional', 'workFromHome']} valuePropName="checked" label={t('workFromHome')}>
          <Switch
            onChange={(val) => form.setFieldsValue({
              optional: {
                workFromHome: val,
              },
            })}
          />
        </Form.Item>

        <Row justify="end" style={{ marginTop: 10 }}>

          <Button
            onClick={discardForm}
            shape="round"
            danger
            type="primary"
            style={{ marginRight: 10 }}
          >
            {t('cancel')}
          </Button>

          <Button
            shape="round"
            type="submit"
            onClick={onPromiseConfirm}
          >
            {t('submit')}
          </Button>
        </Row>
      </Form>

    </Card>
  )
}

JobForm.propTypes = {
  jobData: PropTypes.shape({
    _id: PropTypes.string,
    name: PropTypes.shape({
      th: PropTypes.string,
      en: PropTypes.string,
    }),
    description: PropTypes.shape({
      th: PropTypes.string,
      en: PropTypes.string,
    }),
    categoryId: PropTypes.number,
    salary: PropTypes.number,
    optional: PropTypes.shape({
      workFromHome: PropTypes.bool,
    }),
  }),
}

JobForm.defaultProps = {
  jobData: null,
}

export default JobForm
