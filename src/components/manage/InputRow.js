import {
  Col, Input, Row, Select,
} from 'antd'
import PropTypes from 'prop-types'
import Text from 'antd/lib/typography/Text'
import styled from 'styled-components'

const InputRow = styled(Row)`
  align-items : center;
  margin-bottom : 10px;
`

const CustomSelect = styled(Select)`
  width : 100%
`
const InputTextRow = ({ label, value, onChange }) => (
  <InputRow>
    <Col span={10}>
      <Text>{label}</Text>
    </Col>
    <Col flex={1}>
      <Input value={value} onChange={onChange} type="text" style={{ borderRadius: 20 }} />
    </Col>
  </InputRow>
)
const InputNumRow = ({ label, value, onChange }) => (
  <InputRow>
    <Col span={10}>
      <Text>{label}</Text>
    </Col>
    <Col flex={1}>
      <Input value={value} onChange={onChange} type="number" style={{ borderRadius: 20 }} />
    </Col>
  </InputRow>
)

InputTextRow.propTypes = {
  label: PropTypes.string.isRequired,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  onChange: PropTypes.func,
}

InputNumRow.propTypes = {
  label: PropTypes.string.isRequired,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  onChange: PropTypes.func,
}

InputTextRow.defaultProps = {
  value: '',
  onChange: undefined,
}

InputNumRow.defaultProps = {
  value: '',
  onChange: undefined,
}

const InputSelectRow = ({
  label, value, onChange, options,
}) => (
  <InputRow>
    <Col span={10}>
      <Text>{label}</Text>
    </Col>
    <Col flex={1}>
      <CustomSelect value={value} onChange={onChange} style={{ width: 'fit' }}>
        {options.map((option) => (
          <CustomSelect.Option value={option.id} key={option.id}>{option.name}</CustomSelect.Option>
        ))}
      </CustomSelect>
    </Col>
  </InputRow>
)

InputSelectRow.propTypes = {
  label: PropTypes.string.isRequired,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  onChange: PropTypes.func,
  options: PropTypes.arrayOf(PropTypes.shape({
    text: PropTypes.string,
    value: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
    ]),
  })),
}

InputSelectRow.defaultProps = {
  value: null,
  onChange: null,
  options: [],
}

export { InputTextRow, InputSelectRow, InputNumRow }
