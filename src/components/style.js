import { Layout, Input as andtInput, Card as antdCard } from 'antd'
import styled from 'styled-components'

const BoxContainer = styled.div`
  padding: 25px;
  box-shadow: 0 0 2px #000000;
  background: #fff;
  border-radius: 10px;
  overflow: auto;
`

const Container = styled(Layout)`
  height: 100vh;
  padding: ${({ p = 0 }) => p}px !important;
  padding-left: ${({ px = 0 }) => px};
  padding-right: ${({ px = 0 }) => px};
  padding-top: ${({ py = 0 }) => py};
  padding-bottom: ${({ py = 0 }) => py}; 
  margin-left: auto;
  margin-right: auto;
  background-color: whitesmoke;
`

const Input = styled(andtInput)`
  border-radius: ${({ rounded = false }) => (rounded ? '20' : '0')}px
`

const Card = styled(antdCard)`
  border-radius: 20px;
  box-shadow: 0px 0px 3px rgb(220, 220, 220);
  height : ${({ height }) => (height ? `${height}px` : 'auto')};
`
export {
  BoxContainer, Container, Input, Card,
}
