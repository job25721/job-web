import { gql } from '@apollo/client'

const createJob = gql`
  mutation CreateJob($nameTh:String!,$nameEn:String!,$desTh:String!,$desEn:String!,$categoryId:Int!,$salary:Int,$workFromHome:Boolean){
    createJob(newJob:{
      name : {
        th : $nameTh
        en : $nameEn
      }
      description : {
        th : $desTh
        en : $desEn
      }
      categoryId : $categoryId
      salary : $salary
      optional : {
        workFromHome : $workFromHome
      }
  }){
      _id
      name{
        th
        en
      }
      description{
        th
        en
      }
      salary
      categoryId
      category{
        id
        name
        code
      }
      optional{
        workFromHome
      }
  }
  }
`

const updateJobById = gql`
  mutation UpdateJobById($id:String!,$nameTh:String,$nameEn:String,$desTh:String,$desEn:String,$categoryId:Int,$salary:Int,$workFromHome:Boolean){
    updateJobById(id:$id,data:{
      name : {
        th : $nameTh
        en : $nameEn
      }
      description : {
        th : $desTh
        en : $desEn
      }
      categoryId : $categoryId
      salary : $salary
      optional : {
        workFromHome : $workFromHome
      }
    }){
      _id
      name {
        th
        en
      }
      description{
        th
        en
      }
      categoryId
      salary
      optional{
        workFromHome
      }
    }
  }
`

const deleteJobByid = gql`
  mutation RemoveJobById($id:String!){
    removeJobById(id:$id)
  }
`

export { createJob, updateJobById, deleteJobByid }
