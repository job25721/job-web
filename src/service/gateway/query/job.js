import { gql } from '@apollo/client'

const getAllJobs = gql`
  query{
    getAllJobs{
      data{
      _id
      name{
        th
        en
      }
      description{
        th
        en
      }
      salary
      category
      optional{
        workFromHome
      }
    }
  }
}
`

const searchJob = gql`
  query SearchJob($keyword:String,$lang:String,$categoryId:Int,$salary:Int,$workFromHome:Boolean){
    searchJob(
      queryParams : {
        keyword: $keyword
        lang: $lang
        categoryId: $categoryId
        salary: $salary
        workFromHome: $workFromHome
      }
    ){
      data{
      _id
      name{
        th
        en
      }
      description{
        th
        en
      }
      salary
      category {
        id
        name
        code
      }
      categoryId
      optional{
        workFromHome
      }
    }
    }
  }
`

const getJobTypes = gql`
  query GetJobTypes($lang:String!){
    getJobTypes(lang:$lang){
      total
      data {
        id
        name
        code
      }
    }
  }
`

const getJobById = gql`
  query GetJobById($id:String!){
    getJobById(id:$id){
      _id
      name {
        th
        en
      }
      description{
        th
        en
      }
      categoryId
      salary
      optional{
        workFromHome
      }
    }
  }

`

export {
  getAllJobs, getJobTypes, searchJob, getJobById,
}
