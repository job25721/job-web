import gql from 'graphql-tag'

const getJobTypeList = gql`
   query getJobTypes($lang:String!){
    getJobTypes(lang:$lang){
      total
      data {
        id
        name
        code
      }
    }
  }
`

export default getJobTypeList
