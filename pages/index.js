import {
  Row, Col, Button, Switch,
} from 'antd'
import { useRouter } from 'next/router'
import { useContext, useEffect } from 'react'
import { useLazyQuery } from '@apollo/client'
import { PlusOutlined } from '@ant-design/icons'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import { useTranslation } from 'next-i18next'
import SearchBox from '../src/components/Index/SearchBox'
import JobList from '../src/components/Index/JobList'

import { Container } from '../src/components/style'
import { searchJob } from '../src/service/gateway/query/job'
import { JobContext } from '../src/lib/reducer'
import { SET_ERROR, SET_JOB } from '../src/lib/types'

const Index = () => {
  const router = useRouter()
  const { state, dispatch } = useContext(JobContext)
  const [loadJob, { loading, error, data }] = useLazyQuery(searchJob, { fetchPolicy: 'network-only' })
  const { t } = useTranslation('common')
  useEffect(() => {
    loadJob({
      variables: {
        ...state.searchQuery,
        lang: router.locale,
      },

    })
  }, [])

  useEffect(() => {
    if (data) {
      dispatch({ type: SET_ERROR, payload: null })
      dispatch({ type: SET_JOB, payload: data.searchJob.data })
    }
    if (error) {
      dispatch({ type: SET_ERROR, payload: error.message })
    }
  }, [data, error])

  return (
    <Container px="100px !important" py="20px !important">
      <Row justify="end" align="middle">
        <Switch
          style={{ marginRight: 20 }}
          checked={router.locale === 'en'}
          onChange={(value) => {
            router.replace(router.locale, value ? 'en' : 'th')
          }}
          checkedChildren="EN"
          unCheckedChildren="TH"
        />

        <Button onClick={() => router.push('/manage')} icon={<PlusOutlined />} shape="round" type="primary">{t('newJob')}</Button>
      </Row>
      <Row style={{ height: '100%' }}>
        <Col span={8} style={{ padding: 10 }}>
          <SearchBox loadJob={loadJob} />
        </Col>
        <Col span={16} style={{ padding: 10 }}>
          <JobList loading={loading} />
        </Col>
      </Row>
    </Container>
  )
}

export const getStaticProps = async ({ locale }) => ({
  props: {
    ...await serverSideTranslations(locale, ['common', 'searchBox', 'jobCard']),
  },
})

export default Index
