import { useLazyQuery } from '@apollo/client'
import { Skeleton, Spin } from 'antd'
import Title from 'antd/lib/typography/Title'
import { useRouter } from 'next/router'
import { useEffect } from 'react'

import { useTranslation } from 'next-i18next'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import JobForm from '../../src/components/manage/JobForm'
import { Container } from '../../src/components/style'
import { getJobById } from '../../src/service/gateway/query/job'

const EditJob = () => {
  const router = useRouter()
  const { id } = router.query
  const [loadGetJobById, {
    loading, data, error,
  }] = useLazyQuery(getJobById)
  const { t } = useTranslation('jobForm')
  useEffect(() => {
    if (id) {
      loadGetJobById({ variables: { id } })
    }
  }, [id])

  if (error) {
    return error.message
  }

  return (
    <Container p={20} px="30% !important">
      <Title>
        {t('editJob')}
      </Title>
      {!loading && data ? (
        <JobForm jobData={data.getJobById} type="update" />
      ) : <Skeleton active /> }
    </Container>
  )
}

export const getStaticPaths = async () => ({
  paths: [], // indicates that no page needs be created at build time
  fallback: true, // indicates the type of fallback
})
export const getStaticProps = async ({ locale }) => ({
  props: {
    ...await serverSideTranslations(locale, ['jobForm']),
  },
})
export default EditJob
