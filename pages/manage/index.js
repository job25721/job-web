import Title from 'antd/lib/typography/Title'
import { useContext, useEffect } from 'react'

import { useTranslation } from 'next-i18next'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import JobForm from '../../src/components/manage/JobForm'
import { Container } from '../../src/components/style'
import { JobContext } from '../../src/lib/reducer'
import { RESET_FORM } from '../../src/lib/types'

const Index = () => {
  const { dispatch } = useContext(JobContext)
  const { t } = useTranslation('jobForm')
  useEffect(() => {
    dispatch({ type: RESET_FORM })
  }, [])
  return (
    <Container p={20} px="25% !important">
      <Title>{t('newJob')}</Title>
      <JobForm type="addNew" />
    </Container>
  )
}

export const getStaticProps = async ({ locale }) => ({
  props: {
    ...await serverSideTranslations(locale, ['jobForm']),
  },
})

export default Index
