/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/prop-types */
import 'antd/dist/antd.css'
import '../styles/vars.css'
import '../styles/global.css'
import '../styles/custom.css'

import { ApolloProvider } from '@apollo/client'
import { useReducer } from 'react'
import { appWithTranslation } from 'next-i18next'

import { useApollo } from '../src/lib/apolloClient'
import { initailState, JobContext, jobReducer } from '../src/lib/reducer'

function MyApp({ Component, pageProps }) {
  const apolloClient = useApollo(pageProps)
  const [state, dispatch] = useReducer(jobReducer, initailState)

  return (
    <JobContext.Provider value={{ state, dispatch }}>
      <ApolloProvider client={apolloClient}>
        <Component {...pageProps} />
      </ApolloProvider>
    </JobContext.Provider>
  )
}

export default appWithTranslation(MyApp)
